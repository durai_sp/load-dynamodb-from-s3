from platformutils.dynamodb_utils import DynamoDBUtils
from s3_dynamodb_parser_dal import ParseS3File
from platformutils.s3_utils import S3Utils
from decimal import *
import re
from io import BytesIO
from gzip import GzipFile
import csv
import json, ast
import collections
import StringIO

class Dashboard:

    def __init__(self, tname, region, level):
        self.db = DynamoDBUtils(tname, region, level)
        self.parse_file = ParseS3File(region, level)
        self.s3 = S3Utils(region, level)

        
    def dashboard(self, bucket, key, table_metadata):
        result = self.parse_file.get_dynamodb_file_data(bucket, key)
        for row in csv.reader(StringIO.StringIO(result)):
            finalData = self.handle_data(row)
            formatDict = dict(zip(table_metadata, finalData))
            finalDict = dict((k, v) for k, v in formatDict.iteritems() if v)
            if finalDict.get('deviceModel'):
                finalDict['deviceModel'] = finalDict.get('deviceModel').replace('#',',')
            if finalDict:
                response = self.db.insert_item_to_dynamo(finalDict)

    def handle_data(self, data):
	for i, x in enumerate(data):
    	    try:
            	data[i] = int(x)
    	    except Exception:
		try:
		    data[i] = Decimal(x)
		except Exception as e:
		    pass
            	pass
	return data
